#!/usr/bin/env python3
import time
import math
import sys
start_time = time.time()
primes = []

DEFAULT_TIME_TO_SEARCH = 4
if len(sys.argv) > 1:
    time_to_search = float(sys.argv[3])
else:
    time_to_search = DEFAULT_TIME_TO_SEARCH

print("Searching for {} seconds".format(time_to_search))

n = 2

while time.time() - start_time < time_to_search:
    is_prime = True
    sqn = math.sqrt(n)
    for p in primes:
        if p > sqn:
            break
        
        if n % p == 0:
            is_prime = False
            break
    if is_prime:
        primes.append(n)
    n += 1

print("{} primes generated in {} seconds".format(
    len(primes), time_to_search))
